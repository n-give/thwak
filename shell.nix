with import <nixpkgs> {};
let
  unstable = import <nixos-unstable> {};
in
  stdenv.mkDerivation {
    name = "elm-env";
    nativeBuildInputs = with unstable; [
      elmPackages.elm
      elmPackages.elm-language-server
      elmPackages.elm-format
      elmPackages.elm-test
      elmPackages.elm-live
    ];
  }
