module Main exposing (main)

-- import Json.Decode exposing (Value, Decoder, field, string)

import Browser exposing (Document)
import Browser.Navigation exposing (Key)
import Html exposing (button, div, h4, option, select, text)
import Html.Attributes exposing (selected, style)
import Html.Events exposing (on, onClick, onInput)
import Http
import Json.Decode exposing (Decoder, field, int, list, map, map2, map3, string)
import Maybe exposing (withDefault)
import Url as Url exposing (Protocol(..))


type alias Round =
    { course : String
    , holes : List Hole
    }


type alias Score =
    Maybe Int


type alias Hole =
    { hole : Int
    , par : Int
    , score : Score
    }


type alias Model =
    { course : String
    , holes : List Hole
    }



-- type Model
--     = Failure Http.Error
--     | Loading
--     | Success User


type Msg
    = UrlChanged Url.Url
    | ExternalLink Browser.UrlRequest
    | ScoreSelected Int String
    | ParSelected Int String



init : flags -> Url.Url -> Key -> ( Model, Cmd Msg )
init _ _ _ =
    ( Model "Test course" (List.map (\n -> Hole n 4 Nothing) (List.range 1 18))
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


view : Model -> Document Msg
view model =
    { title = "Thwak"
    , body =
        [ div []
            [ div
                [ style "margin" "1.5em"
                ]
                [ text model.course ]
            , div
                [ style "display" "grid"
                , style "grid-template-columns" "3em 1fr 3em"
                ]
                [ div
                    [ style "display" "grid"
                    , style "grid-template-rows" "repeat(4, 1fr)"
                    ]
                    [ div [] [ text "Hole" ]
                    , div [] [ text "Par" ]
                    , div [] [ text "Score" ]
                    , div [] []
                    ]
                , div
                    [ style "display" "grid"
                    , style "grid-template-columns" "repeat(18, 1fr)"
                    ]
                    (List.map viewHole model.holes)
                , div
                    [ style "display" "grid"
                    , style "grid-template-rows" "repeat(4, 1fr)"
                    ]
                    [ div [] []
                    , div []
                        [ text <|
                            holeSum (\h -> h.par) model.holes
                        ]
                    , div []
                        [ text <|
                            holeSum (\h -> withDefault 0 h.score) model.holes
                        ]
                    , div []
                        [ text <|
                            holeSum
                                (\h -> withDefault h.par h.score - h.par)
                                model.holes
                        ]
                    ]
                ]
            ]
        ]
    }


holeSum : (Hole -> Int) -> List Hole -> String
holeSum accessor =
    String.fromInt << List.sum << List.map accessor


viewHole : Hole -> Html.Html Msg
viewHole h =
    let
        rightAlignedNumber n =
            div
                [ style "text-align" "right" ]
                [ text (String.fromInt n) ]
    in
    div
        [ style "margin-right" "1em" ]
        [ div
            [ style "display" "grid"
            , style "grid-template-rows" "repeat(4, 1fr)"
            ]
            [ rightAlignedNumber h.hole
            , select
                [ onInput (ParSelected h.hole) ]
                (showPars h.par)
            , select
                [ onInput (ScoreSelected h.hole) ]
                (showScores (withDefault -1 h.score))
            , rightAlignedNumber (withDefault h.par h.score - h.par)
            ]
        ]


generateOptions : Int -> List Int -> List (Html.Html msg)
generateOptions s =
    List.map
        (\o -> option [ selected (o == s) ] [ text (String.fromInt o) ])


showPars : Int -> List (Html.Html msg)
showPars n =
    generateOptions n (List.range 3 5)


showScores : Int -> List (Html.Html msg)
showScores n =
    generateOptions n (List.range 1 10)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ScoreSelected h s ->
            ( { model
                | holes =
                    List.map
                        (\oh ->
                            if oh.hole == h then
                                { oh | score = String.toInt s }

                            else
                                oh
                        )
                        model.holes
              }
            , Cmd.none
            )

        ParSelected h p ->
            ( { model
                | holes =
                    List.map
                        (\oh ->
                            if oh.hole == h then
                                { oh | par = withDefault 4 <| String.toInt p }

                            else
                                oh
                        )
                        model.holes
              }
            , Cmd.none
            )

        _ ->
            ( model, Cmd.none )



-- update : Msg -> Model -> ( Model, Cmd Msg )
-- update msg model =
--     case msg of
--         GotUser newNameR ->
--             case newNameR of
--                 Ok newName ->
--                     ( Success newName, Cmd.none )
--
--                 Err s ->
--                     ( Failure s, Cmd.none )
--
--         GetUser ->
--             ( Loading
--             , Http.get
--                 { url = "http://localhost:8081/person/1"
--                 , expect = Http.expectJson GotUser userDecoder
--                 }
--             )
--
--         UrlChanged _ ->
--             ( model, Cmd.none )
--
--         ExternalLink _ ->
--             ( model, Cmd.none )


main : Program () Model Msg
main =
    Browser.application
        { init = init -- : flags -> Url -> Key -> ( model, Cmd msg )
        , view = view -- : model -> Document msg
        , update = update -- : msg -> model -> ( model, Cmd msg )
        , subscriptions = subscriptions -- : model -> Sub msg
        , onUrlRequest = ExternalLink -- : UrlRequest -> msg
        , onUrlChange = UrlChanged -- : Url -> msg
        }
